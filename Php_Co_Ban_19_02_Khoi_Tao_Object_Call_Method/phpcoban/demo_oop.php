<?php
class Number {
    public $numberA = 1;
    public $numberB = 2;

    function getTotal($number1, $number2) {
        return $number1 + $number2;
    }
}

$numberObj = new Number();
echo 7;
echo "<br>";

var_dump($numberObj);
echo "<br>";

$total = $numberObj->getTotal($numberObj->numberA, $numberObj->numberB);
var_dump($total);
echo "<br>";